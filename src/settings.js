'use strict';
const fs = require('fs');
const path = require('path');
const settingsFile = path.resolve(__dirname, '..', 'settings.json')

let settings = {}
if(fs.existsSync(settingsFile)) {
  try {
    let settingsFileContents = fs.readFileSync(settingsFile)
    settings = JSON.parse(settingsFileContents)
  } catch (err) {
    console.error(`Unable to read settings, but file existed ${err} ${settingsFileContents}`);
  }  
}

settings.cacheDir = settings.cacheDir || path.resolve(__dirname, '..', 'cache')
settings.presetDir = settings.presetDir || path.resolve(__dirname, '..', 'presets')
settings.announceVolume = settings.announceVolume || 40
settings.autoShufflePlaylists = settings.autoShufflePlaylists === null ? true : settings.autoShufflePlaylists
settings.i2cDevice = settings.i2cDevice || '/dev/i2c-1'

if (!fs.existsSync(settings.cacheDir)) {
  try {
    fs.mkdirSync(settings.cacheDir);
  } catch (err) {
    console.error(`Could not create cache directory ${settings.cacheDir}, please create it manually for all features to work.`);
  }
}

module.exports = settings;
