const pn532 = require('pn532');
// const i2c = require('i2c');

const NFC = (player, api, settings) => {
  const wire = new i2c(pn532.I2C_ADDRESS, {device: settings.i2cDevice});
  const rfid = new pn532.PN532(wire);

  rfid.on('ready', function() {
    console.log('Listening for a tag scan...');
    rfid.on('tag', function(tag) {
        if (tag) console.log('tag:', tag.uid);
    });
  });
}

module.exports = NFC