const SonosSystem = require('sonos-discovery');
const SonosAPI = require('./sonos-api');
const playerFinder = require('./playerFinder');
const settings = require('./settings');

const discovery = new SonosSystem(settings);
const api = new SonosAPI(discovery, settings);

function handle(tag) {
  let player = playerFinder(discovery, settings.player)

  if(tag.match(/^spotify:/).index > -1) {
    api.spotify(player, 'now', tag)
  }
}

module.exports = handle;
