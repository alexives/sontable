const requireDir = require('sonos-http-api/lib/helpers/require-dir');
const path = require('path');
const actionsDir = path.resolve(path.dirname(require.resolve('sonos-http-api')), 'actions')

function SonosAPI(discovery, settings) {
  this.discovery = discovery;

  let self = this

  // this handles registering of all actions
  this.registerAction = function (action, handler) {
    self[action] = handler;
  };

  this.getPort = function() {}

  //load modularized actions
  requireDir(actionsDir, (requireCallback) => {
    try {
      requireCallback(this);
    } catch (err) {
      console.log(err)
    }
  });

  this.handleAction = function(options) {
    var player = options.player;

    if (!actions[options.action]) {
      return Promise.reject({ error: 'action \'' + options.action + '\' not found' });
    }

    return actions[options.action](player, options.values);
  }
}

module.exports = SonosAPI;