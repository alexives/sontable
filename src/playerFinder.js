function playerFinder(discovery, name) {
  let value = null
  discovery.players.forEach(player => {
    if(player.roomName == name) {
      value = player
    }
  });
  return value
}

module.exports = playerFinder
